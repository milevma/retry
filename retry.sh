retry() {
  local n=1
  local max=5
  local delay=300
  while true; do
    "$@" && break || {
      if [[ $n -lt $max ]]; then
        ((n++))
        echo "############################################################"
        echo "Command failed. Attempt $n/$max:"
        sleep $delay
      else
        echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo "Retrying has failed after $n attempts."
        exit 1
      fi
    }
  done
}

