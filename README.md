# Retry

This project gives a retry logic to bash commands. Main purpose is to use it when you are dealing with APIs that can sometimes timeout. This functionality is mostly helpful in CI/CD pipelines.

## Getting started

All you need to do is to simply source the [retry.sh](https://gitlab.com/milevma/retry/-/blob/main/retry.sh) in your shell, script or CI/CD pipeline.

#### Example

```
git clone https://gitlab.com/milevma/retry.git
cd retry
source ./retry.sh
```

then you can execute a command and if the command fails the retry logic will be applied:

```
martin@my-pc retry % retry aws s3 ls blabla

An error occurred (AccessDenied) when calling the ListObjectsV2 operation: Access Denied
############################################################
Command failed. Attempt 2/5:

An error occurred (AccessDenied) when calling the ListObjectsV2 operation: Access Denied
############################################################
Command failed. Attempt 3/5:

An error occurred (AccessDenied) when calling the ListObjectsV2 operation: Access Denied
############################################################
Command failed. Attempt 4/5:

An error occurred (AccessDenied) when calling the ListObjectsV2 operation: Access Denied
############################################################
Command failed. Attempt 5/5:

An error occurred (AccessDenied) when calling the ListObjectsV2 operation: Access Denied
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Retrying has failed after 5 attempts.
```

By default the retry attempts are 5 and they are delayed by 5 minutes.
